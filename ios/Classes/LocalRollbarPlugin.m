#import "LocalRollbarPlugin.h"
#if __has_include(<local_rollbar/local_rollbar-Swift.h>)
#import <local_rollbar/local_rollbar-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "local_rollbar-Swift.h"
#endif

@implementation LocalRollbarPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftLocalRollbarPlugin registerWithRegistrar:registrar];
}
@end
