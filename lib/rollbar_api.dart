import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dart_ipify/dart_ipify.dart';
import 'package:http/http.dart' as http;
import 'package:local_rollbar/flutter_rollbar.dart';

class RollbarApi {
  final http.Client _client = http.Client();

  Future<http.Response> sendReport({
    required String accessToken,
    required String message,
    required List<RollbarTelemetry> telemetry,
    Map? clientData,
    RollbarPerson? person,
    String? environment,
    String? ipAddress,
    RollbarLogLevel? levelData,
  }) async {

    final ipv4 = await Ipify.ipv4();

    var level = levelData ?? RollbarLogLevel.ERROR;
    var post = await _client.post(
      Uri.parse('https://api.rollbar.com/api/1/item/'),
      body: json.encode(
        {
          'access_token': accessToken,
          'data': {
            'level': level.name,
            'environment': environment,
            'platform': Platform.isAndroid ? 'android' : 'ios',
            'framework': 'flutter',
            'language': 'dart',
            'body': {
              'message': {
                'body': message,
              },
              'telemetry': telemetry.map((item) => item.toJson()).toList(),
            },
            "request": {"user_ip": ipv4},
            'person': person?.toJson(),
            'client': clientData,
            'notifier': {
              'name': 'flutter_rollbar',
              'version': '0.1.0+1',
            }
          }
        },
      ),
    );

    return post;
  }
}
