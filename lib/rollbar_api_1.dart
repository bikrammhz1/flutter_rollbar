// import 'dart:async';
// import "package:dio/dio.dart";
//
// class RollbarApi1 {
//
//   static Future<Response> sendReport({required String accessToken}) {
//     var client = Client();
//
//     return client.post(
//       Uri.parse('https://api.rollbar.com/api/1/item/'),
//       body: json.encode(
//         {
//           'access_token': accessToken,
//           'data': {
//             "data": {
//               "notifier": {"version": "0.2.0-beta", "name": "rollbar-dart"},
//               "environment": "QA",
//               "person": {
//                 "id": "flutter@gmail.com",
//                 "name": "flutter",
//                 "email": "flutter@gmail.com"
//               },
//               "client": {
//                 "locale": "en_GB",
//                 "hostname": "localhost",
//                 "os": "android",
//                 "os_version": "1",
//                 "dart": {"version": "2.17.6 (stable)"}
//               },
//               "platform": "ios",
//               "language": "dart",
//               "level": "critical",
//               "timestamp": 1672534995217374,
//               "body": {
//                 "message": {"body": "People log error"}
//               },
//               "framework": "flutter",
//               "code_version": "27",
//               "server": {"root": "app.krispcall"}
//             }
//           }
//         },
//       ),
//     );
//     // return sendReport(accessToken: accessToken);
//   }
// }
